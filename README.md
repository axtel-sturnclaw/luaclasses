Luaclasses - Classes for Lua
===========

Luaclasses adds OOP-like classes to Lua.  Usage is as simple as:

    class = require 'class'

    Class1 = class()
    function Class1:test(msg) print(msg) end

    Instance1 = Class1()
    Instance1:test('This is a test!')

    > This is a test!

The Details:
------------

Luaclasses is a fully featured, multiple-inheritance capable, class library for
Lua, usable for just about any Object Oriented/Class-Based system you can make work in plain Lua.

Taking design cues from Lua, Luaclasses is designed around the core idea of a
class-instance model, leaving all other specifics to the user.  Interfaces are
as simple as a class with empty functions, multiple inheritance is just a matter
of specifiying a class in your super[...] lookups, and calls to super:something()
*just work*.

But the best bit?  All this happens **during runtime**.  No compilation, no
C modules, just plain Lua.  And it's lightweight, too.

On an x86 system, luaclasses' initial memory footprint is less than 16KB,
and new classes and instances are less than 400 bytes per.
For comparison, a single, empty, locally-scoped closure (`function() end`) is approximately 200 B.

It's fast too.  Luaclasses has been designed to keep out of the way as much as
possible while still delivering a viable class system.  Superclass lookups in
an instance cache the returned value by default, reducing the number of metamethod calls.
Everything runs as close to plain Lua tables as possible.

C Implementation:
--------------------------------

For those with performance critical applications, or C code that needs to work with classes without
dropping to Lua, Luaclasses now ships with a C implementation of the class library.

The C implementation has full feature-parity with the Lua implementation, and can be used
*without change* in any code using the Lua implementation.

To use the C implementation, simply change `require 'class'` to `require 'lclass'`.  That's it.

In a simple test case utilizing a pre-existing application, using Lua 5.3.x on two different Linux
distros, the C implementation completes the test in approximately half the time of the Lua
implementation.

API
=============================

Luaclasses provides one function: `class`.  This is an anonymous function
returned by `require()`, so you can call it anything you want.  I refer to it as
`class` throughout this document.

`class` can be called in two ways.  The first is `class()`, which creates and
returns an empty class.
The second is `class(super1, super2, ...)`, which creates a class inheriting
from each valid class passed to it.  Any non-class object will simply be 
ignored.

Classes have a few default fields, required to implement class-like
functionality.  These are as follows:
* `super` - A convenience table, this always refers to the superclass of the
function accessing it.  Invoke like `self.super:something()`.  Using `:` to call
a function passes the instance or class calling the function, rather than the
supertable.
* `__super` - The actual supertable of the instance or class.  `self.something`
lookups use this table.
* `__is_a` - Table storing a flattened inheritance tree.  If an object inherits
from another class, that class is a key in the table.
* `instanceof` - Inheritance check function.  Invoke as `obj:instanceof(obj1, 
obj2)`  Returns true if `obj` inherits from all valid classes passed.

To create an instance of a class, simply call the class like so: `Class1(...)`.
This creates an instance, and calls the first initialization function, passing
the new instance, and all arguments received. (NOTE: If using the Lua
implementation, this may be configured to automatically call all initialization
functions, working down from the furthest superclass.  It may also be configured
to pass the class as the first argument, before the instance and args.)

The initialization function name defaults to `__init`, although that may be
redefined in both the Lua and C implementations.

Classes may provide operator overrides by creating functions with
names as [defined by Lua](https://www.lua.org/manual/5.3/manual.html#2.4), with
a few exceptions:
* `__call` will only be called on instances.
* `__index` and `__newindex` are ignored.  Getters and setters *may* be
supported in the future.
* `__tostring`, while not listed in that specific section, is supported.

Examples:
=====================

Inheritance:
------------

    Class1 = class()
    Class2 = class(Class1)
    Class3 = class(Class2)

Invocation of Superclass Methods:
---------------------------------

    function Class1:test(msg) print('Class1', msg) end
    function Class2:test(msg) self.super:test(msg); print('Class2') end
    function Class3:test(msg) self.super:test(msg); print('Class3') end

    Class3:test('TEST!')

    > Class1    TEST!
      Class2
      Class3

Multiple inheritance and Class-Specific Super[...] Lookups:
---------------------

    Class2 = class(Class1)
    Class2_1 = class(Class1)
    Class3 = class(Class2, Class2_1)

    Class2.test = 'Class2 Test'; Class2_1.test = 'Class2_1 Test'

    print(Class3.test, Class3.super.test)
    > Class2 Test   Class2 Test

    print(Class3.super[Class2_1].test)
    > Class2_1 Test

Operator Overrides:
-------------------

    function Class1.__add(a,b) print('Test!'); return 5 end
    i = Class3(); print(i + i)
    
    > Test!
      5
    
    function Class1:__tostring() return "Class 1" end
    print(Class1)
    
    > Class 1

Inheritance Checking:
--------------

    Class1 = class()
    Class2 = class()

    i = Class2()

    print(Class1:instanceof(Class2), Class1:instanceof(Class1),
     i:instanceof(Class2))

    > false  true  true

Type Checking:
--------------

    print(getmetatable( i ))
    > instance

    print(getmetatable( Class1 ))
    > class

    print(getmetatable( Class1.super ))
    > super
