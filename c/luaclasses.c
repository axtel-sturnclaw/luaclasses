/*
 * LuaClasses.c - Lua-facing API
 * Copyright 2016 Web eWorks, LTD
 */

#include "luaclasses.h"

#define LUACLASSES_API extern

static int __newclass (lua_State *L)
{
	int final = 0;
	int name = 0;
	if (lua_gettop(L) > 0){
		if (lua_type(L, 1) == LUA_TSTRING) {
			if (strcmp("final", lua_tostring(L, 1)) != 0) {
				// Not final, treat as class name
				name = 1;
			}
			else if (lua_gettop(L) == 1) {
				// final
				final = 1;
			}
		}
		if (lua_gettop(L) > 0 && !final) {
			if (lua_type(L, -1) == LUA_TSTRING && strcmp("final", lua_tostring(L, -1)) == 0) {
				final = 1;
			}
			else if (lua_type(L, -1) == LUA_TBOOLEAN && lua_toboolean(L, -1)) {
				final = 1;
			}
		}
		if (final) lua_pop(L, 1);
	}
	return lclass_newclass(L, lua_gettop(L) - name, name, final);
}

static int __functionwrapper_postcall (lua_State *L, int status, lua_KContext ctx)
{
	// Not an object method call.
	if (!ctx) return lua_gettop(L);

	// stack -> obj.super, class.super, class.super[0], (...)
	lua_rotate(L, 1, -3);
	// stack -> (...), obj.super, class.super, class.super[0]

	// obj.super = old obj.super
	lua_rawgeti(L, -3, 0);
	// stack -> (...), class.super, class.super[0], obj, obj.super
	lua_rotate(L, -4, -1);
	lua_setfield(L, -2, "super");
	// pop the object
	lua_pop(L, 1);
	// stack -> (...), class.super, class.super[0]

	// class.super[0] = old class.super[0]
	lua_seti(L, -2, 0);
	// pop class.super
	lua_pop(L, 1);

	if (status > LUA_YIELD) {
	lua_error(L);
	}
	return lua_gettop(L);
}

static int __functionwrapper (lua_State *L)
{
	int args = lua_gettop(L);
	// upvalues -> obj.super, func, orig_class

	lua_pushvalue(L, lua_upvalueindex(2));
	lua_insert(L, 1);

	// Get the instance.
	lua_rawgeti(L, lua_upvalueindex(1), 0);

	// if we are calling this as self:func() or self.super:func()
	// we need to replace the implicit self argument with the instance.
	if (lua_rawequal(L, 2, -1) || lua_rawequal(L, 2, lua_upvalueindex(1))) {
	// Store the old obj.super
	lua_getfield(L, -1, "super");
	lua_insert(L, 1);

	// Get the old class.super
	lua_getfield(L, lua_upvalueindex(3), "super");
	// Store the old class.super[0]
	lua_geti(L, -1, 0);
	lua_insert(L, 2);
	// Store class.super
	lua_pushvalue(L, -1);
	lua_insert(L, 2);

	// Copy the object
	lua_pushvalue(L, -2);
	// class.super[0] = obj
	lua_seti(L, -2, 0);
	// obj.super = class.super
	lua_setfield(L, -2, "super");
	// replace the old first value from obj:call()

	// stack -> old obj.super, old class.super, old.class.super[0], old func,
	// (cont) -> old first arg, (...), new obj
	lua_replace(L, 5);
	// call it.
	return __functionwrapper_postcall(L,
		lua_pcallk(L, args, LUA_MULTRET, 0, 1, __functionwrapper_postcall),
		1);
	}
	// otherwise, we are just calling it normally.
	else {
	// remove the supertable
	lua_pop(L, 1);
	// call the function.
	lua_callk(L, args, LUA_MULTRET, 0, __functionwrapper_postcall);
	return lua_gettop(L);
	}
}

static int lclass_superindex (lua_State *L)
{
	const int self_idx = lua_absindex(L, -2);
	const int key_idx = lua_absindex(L, -1);

	if (lua_isnumber(L, key_idx)) return 0;

	// obj.super[SomeSuperClass].val
	if (lclass_isclass(L, key_idx)) {
	lua_geti(L, self_idx, 0);
	lua_pushvalue(L, key_idx);
	if (lclass_inheritsfrom(L, -2)) {
		lua_newtable(L);
		lua_insert(L, key_idx + 1);
		lua_seti(L, -3, 1);
		lua_seti(L, -2, 0);
		luaL_setmetatable(L, "lclass.super");
		return 1;
	}
	return 0;
	}

	lua_pushvalue(L, key_idx);
	if (!lclass_searchsuper(L, self_idx, TRUE)) return 0;
	// Functions need wrappers, everything else is fine.
	if (lua_type(L, -2) != LUA_TFUNCTION) {
	lua_pop(L, 1);
	return 1;
	}

	lua_remove(L, key_idx);
	// stack -> obj.super, func, orig_class
	lua_pushcclosure(L, __functionwrapper, 3);
	// return our wrapper function.
	return 1;
}

static int lclass_supernewindex (lua_State *L)
{
	(void) L;
	/* Deliberately left blank */
	return 0;
}

static int lclass_classindex (lua_State *L)
{
	lua_getfield(L, 1, "__super");
	lua_replace(L, 1);
	lua_gettable(L, 1);
	return 1;
}

static int lclass_classcall (lua_State *L)
{
	luaL_argcheck(L, lclass_isclass(L, 1), 1, "");
	lclass_newinstance(L, 1);
	lua_replace(L, 1);
	return lclass_initinstance(L, 1, lua_gettop(L) - 1);
}

static int lclass_instindex (lua_State *L)
{
	if (!lclass_isinstance(L, 1)) return 0;
	int is_ctor = lua_isstring(L, -1) && strcmp(lua_tostring(L, -1), LUACLASSES_CTORNAME) == 0;
	lua_getfield(L, -2, "__super");
	lua_pushvalue(L, -2);
	if (lua_gettable(L, -2) != LUA_TNIL && !is_ctor) {
	lua_remove(L, -2);
	lua_pushvalue(L, -1);
	lua_insert(L, 2);
	lua_settable(L, -4);
	}
	return 1;
}

luaL_Reg supermt[] = {
	{"__index", lclass_superindex},
	{"__newindex", lclass_supernewindex},
	{NULL, NULL}
};

luaL_Reg classmt[] = {
	{"__index", lclass_classindex},
	{"__call", lclass_classcall},
	{NULL, NULL}
};

luaL_Reg instmt[] = {
	{"__index", lclass_instindex},
	{NULL, NULL}
};

LUACLASSES_API int luaopen_lclass(lua_State *L)
{
	luaL_newmetatable(L, "lclass.super");
	luaL_setfuncs(L, supermt, 0);
	lua_pushstring(L, "super");
	lua_setfield(L, -2, "__metatable");
	lua_pop(L, 1);

	luaL_newmetatable(L, "lclass.class");
	luaL_setfuncs(L, classmt, 0);
	lua_pushstring(L, "class");
	lua_setfield(L, -2, "__metatable");
	lclass_setoperators(L);
	lua_pop(L, 1);

	luaL_newmetatable(L, "lclass.instance");
	luaL_setfuncs(L, instmt, 0);
	lua_pushstring(L, "instance");
	lua_setfield(L, -2, "__metatable");
	lclass_setoperators(L);
	lua_pop(L, 1);

	lua_pushcfunction(L, __newclass);

	return 1;
}
