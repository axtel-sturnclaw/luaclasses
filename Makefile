CP = cp
LUAVER = 5.3
INSTDIR = /usr/local/share/lua/$(LUAVER)

.PHONY: install uninstall module module-clean

module:
	$(MAKE) -C c/

module-clean:
	$(MAKE) -C c/ clean

install: $(module)
	$(CP) init.lua $(INSTDIR)/class.lua
	$(MAKE) -C c/ install

uninstall:
	$(RM) $(INSTDIR)/class.lua
	$(MAKE) -C c/ uninstall
