/*
 * file luaclasses_public.c
 * Public API Implementation for LuaClasses
 * Copyright 2016 Web eWorks, LTD under the terms of the MIT License.
 */

#include "luaclasses.h"

static int __instanceof (lua_State *L)
{
	if (!lua_gettop(L) || !lclass_isobject(L, 1)) {
		lua_pop(L, lua_gettop(L));
		lua_pushnil(L);
		lua_pushfstring(L, "Invalid class object to check inheritance");
		return 2;
	}
	if (lua_gettop(L) < 2) {
		lua_pop(L, lua_gettop(L));
		lua_pushnil(L);
		lua_pushfstring(L, "No superclasses passed!");
		return 2;
	}
	while (lua_gettop(L) > 1) {
		lua_rotate(L, 2, lua_gettop(L) - 2);
		if (!lclass_inheritsfrom(L, 1)) {
			lua_pop(L, lua_gettop(L));
			lua_pushboolean(L, 0);
			return 1;
		}
		lua_pop(L, 1);
	}
	lua_pop(L, lua_gettop(L));
	lua_pushboolean(L, 1);
	return 1;
}

static int __initinstancecont (lua_State *L, int status, lua_KContext ctx)
{
	return status == LUA_OK;
}

int lclass_searchsuper (lua_State *L, int index, int raw)
{
	const int class_idx = lua_absindex(L, index);
	const int key_idx = lua_absindex(L, -1);
	/* raw indicates that we are directly passing the
		supertable of the object we want to look in. */

	/* If we are attempting to lookup a nil key, or not looking in a class object,
		then return nothing. */
	if (lua_type(L, key_idx) == LUA_TNIL ||
	 (!raw && !lclass_isobject(L, class_idx))) {
		lua_pop(L, 1);
		return 0;
	}

	if (raw) lua_pushvalue(L, class_idx);
	else lua_getfield(L, class_idx, "__super");

	/* Stack: ..., key, supert */

#if LUACLASSES_BFIRST

	/* breadth-first search */
	lua_checkstack(L, 7);

	int srt_idx = key_idx + 1;
	int srt_len = 1;
	int srt_i = 1;

	lua_newtable(L);
	lua_insert(L, srt_idx);
	lua_seti(L, -2, srt_i);

	lua_getglobal(L, "table");

	/* Stack: ..., key, searcht, tablelib */
	while ((srt_len = luaL_len(L, srt_idx)) > 0) {
		/* Call t = table.remove(searcht) */
		lua_getfield(L, srt_idx+1, "remove");
		lua_pushvalue(L, srt_idx);
		lua_pushinteger(L, srt_i);
		/* Stack: ..., key, searcht, tablelib, table.remove, searcht, 1 */
		lua_call(L, 2, 1);

		/* If it is somehow not a table, skip it. */
		if (lua_type(L, -1) != LUA_TTABLE) {
			lua_pop(L, 1);
			continue;
		}

		/* Check the classes in this supertable */
		int sidx = lua_gettop(L);
		for (int i = 1; i <= luaL_len(L, sidx); ++i) {
			/* Stack: ..., key, searcht, tablelib, __super */
			/* Get the class */
			lua_rawgeti(L, sidx, i);
			if (!lclass_isobject(L, -1)) {
				lua_pop(L, 1);
				continue;
			}

			/* Check for a value */
			lua_pushvalue(L, key_idx);
			/* Stack: ..., key, searcht, tablelib, __super, class, key */
			if (lua_rawget(L, sidx+1) != LUA_TNIL) {
				/* Found a value */
				lua_insert(L, key_idx);
				lua_insert(L, key_idx+1);
				/* Stack: ..., value, class, key, searcht, tablelib, __super */
				lua_pop(L, 4);
				/* Stack: ..., value, class */
				return TRUE;
			}
			else {
				/* No value; pop the nil from lua_rawget */
				lua_pop(L, 1);

				/* If this class's has superclasses, push its supertable */
				lua_pushstring(L, "__super");
				lua_rawget(L, sidx+1);

				/* Remove the class value from the stack*/
				lua_remove(L, -2);

				/* Stack: ..., key, searcht, tablelib, __super, class.__super */
				if (luaL_len(L, -1) > 0) {
					/* Call table.insert(searcht, i, class.__super) */
					lua_getfield(L, srt_idx+1, "insert");
					lua_pushvalue(L, srt_idx);
					lua_pushinteger(L, srt_i);
					lua_rotate(L, sidx+1, -1);

					/* Stack: ..., key, searcht, tablelib, __super, table.insert, searcht, i, class.__super */
					lua_call(L, 3, 0);
					++srt_i;
				}
				else {
					lua_pop(L, 1);
				}
			}
		}
		/* Stack: ..., key, searcht, tablelib, __super */
		lua_pop(L, 1);

		/* Pass complete, start a new pass */
		if (srt_i > luaL_len(L, srt_idx)) srt_i = 1;
	}

	/* Our segment of the stack is clear. */
	lua_pop(L, 3);

#else

	/* Depth-first search */
	lua_checkstack(L, 5);

	int superlen = luaL_len(L, -1);
	if (superlen < 1) {
		lua_pop(L, 2);
		return FALSE;
	}

	lua_insert(L, key_idx);
	for (int i = 1; i <= superlen; i++) {
		lua_rawgeti(L, key_idx, i);
		int superclass_idx = lua_gettop(L);
		if (!lclass_isobject(L, -1)) {
			lua_pop(L, 1);
			continue;
		}
		/* see if the key is directly present in the superclass. */
		lua_pushvalue(L, key_idx + 1);
		int t = lua_rawget(L, superclass_idx);
		if (t != LUA_TNIL) {
			/* transform the stack:
				super, key, class, value => value, class
			*/
			lua_insert(L, key_idx);
			lua_insert(L, key_idx + 1);
			lua_pop(L, 2);
			return TRUE;
		}
		/* if not, we remove the nil value from the stack, pushing the key in it's place. */
		lua_pop(L, 1);
		lua_pushvalue(L, key_idx + 1);

		/* recursion into supersuperclasses */
		if (lclass_searchsuper(L, superclass_idx, FALSE)) {
			/* transform the stack:
				super, key, class, value, foundclass => value, foundclass
			*/
			lua_insert(L, key_idx);
			lua_insert(L, key_idx);
			lua_pop(L, 3);
			return TRUE;
		}
		/* a failed lookup removes the key from the stack without pushing
			anything in it's place, so we only need to remove the class. */
		lua_pop(L, 1);
	}
	lua_pop(L, 2);

#endif

	return FALSE;
}

int lclass_newclass (lua_State *L, int superclasses, int name, int final)
{
	lua_checkstack(L, 5);

	int s_idx;
	// the index of the first superclass
	if (superclasses) s_idx = lua_absindex(L, -(superclasses));
	// the index of the value before the new class
	else s_idx = lua_gettop(L);

	// new class
	lua_newtable(L);

	// set the __type field
	lua_pushstring(L, "class");
	lua_setfield(L, -2, "__type");

	// new __is_a table
	lua_newtable(L);
	// add the new class
	lua_pushvalue(L, -2);
	lua_pushboolean(L, TRUE);
	lua_settable(L, -3);
	// set the __is_a table
	lua_setfield(L, -2, "__is_a");

	// install the instanceof function
	lua_pushcfunction(L, __instanceof);
	lua_setfield(L, -2, "instanceof");

	if (final) {
		// set the __final field
		lua_pushboolean(L, TRUE);
		lua_setfield(L, -2, "__final");
	}

	if (name) {
		if (superclasses) {
			// move the name to the top of the stack.
			lua_rotate(L, s_idx-1, -1);
			s_idx -= 1;
		}
		else {
			// with no superclasses, s_idx points to the name value
			lua_rotate(L, s_idx, -1);
		}
		// set the __name field.
		lua_setfield(L, -2, "__name");
	}

	// new supertable
	lua_newtable(L);
	// install the class at super[0]
	lua_pushvalue(L, -2);
	lua_seti(L, -2, 0);

	for (;superclasses > 0 && !lua_rawequal(L, -2, s_idx); --superclasses) {
		if (lclass_isclass(L, s_idx)) {
			if (lua_getfield(L, s_idx, "__final")) {
				lua_pop(L, 1); lua_remove(L, s_idx);
				continue;
			}
			else lua_pop(L, 1);
			lua_pushvalue(L, s_idx);
			lua_getfield(L, -1, "__is_a");
			int t = lua_absindex(L, -1);
			lua_getfield(L, -4, "__is_a");
			lua_pushnil(L);
			while (lua_next(L, t)) {
				lua_pop(L, 1);
				lua_pushvalue(L, -1);
				lua_pushboolean(L, TRUE);
				lua_settable(L, -4);
			}
			lua_remove(L, t);
			lua_pushvalue(L, -2);
			lua_pushboolean(L, TRUE);
			lua_settable(L, t);
			lua_pop(L, 1);
			lua_seti(L, -2, luaL_len(L, -2) + 1);
		}
		lua_remove(L, s_idx);
	}

	luaL_setmetatable(L, "lclass.super");
	lua_pushvalue(L, -1);

	lua_setfield(L, -3, "__super");
	lua_setfield(L, -2, "super");

	luaL_setmetatable(L, "lclass.class");
	return 1;
}

int lclass_newinstance (lua_State *L, int index)
{
	if (!lclass_isclass(L, index)) return 0;
	lua_checkstack(L, 3);
	lua_newtable(L);
	luaL_setmetatable(L, "lclass.instance");
	lua_pushstring(L, "instance");
	lua_setfield(L, -2, "__type");

	lua_newtable(L);
	lua_pushvalue(L, -2);
	lua_seti(L, -2, 0);
	lua_pushvalue(L, index);
	lua_seti(L, -2, 1);
	luaL_setmetatable(L, "lclass.super");

	lua_pushvalue(L, -1);
	lua_setfield(L, -3, "__super");
	lua_setfield(L, -2, "super");

	return 1;
}

int lclass_initinstance(lua_State *L, int index, int numvars)
{
	index = lua_absindex(L, index);
	if (!lclass_isinstance(L, index)) {
		if (numvars) lua_pop(L, numvars);
		return 0;
	}
	int s_idx = -numvars;
	if (!s_idx) s_idx = -1;
	s_idx = lua_absindex(L, s_idx);
	if(lua_getfield(L, index, LUACLASSES_CTORNAME) != LUA_TFUNCTION) {
		lua_pop(L, numvars + 1);
		return 1;
	}
	lua_pushvalue(L, index);
	if (numvars) {
		lua_insert(L, s_idx); // Push the instance before the vars
		lua_insert(L, s_idx); // Push the function to the beginning
	}
	lua_callk(L, numvars + 1, 0, 0, __initinstancecont);
	return 1;
}

int lclass_isobject (lua_State *L, int index)
{
	index = lua_absindex(L, index);
	lua_checkstack(L, 3);
	if (lua_istable(L, index)) {
		lua_pushstring(L, "__super");
		lua_rawget(L, index);
		if (lua_getmetatable(L, -1)) {
			luaL_getmetatable(L, "lclass.super");
			if (lua_rawequal(L, -1, -2)) {
				lua_pop(L, 3);
				return TRUE;
			}
			lua_pop(L, 3);
		}
		else lua_pop(L, 1);
	}
	return FALSE;
}

int lclass_isclass (lua_State *L, int index)
{
	index = lua_absindex(L, index);
	lua_checkstack(L, 2);
	if (lclass_isobject(L, index)) {
		if (lua_getmetatable(L, index)) {
			luaL_getmetatable(L, "lclass.class");
			if (lua_rawequal(L, -1, -2)) {
				lua_pop(L, 2);
				return TRUE;
			}
		}
	}
	return FALSE;
}

int lclass_isinstance (lua_State *L, int index)
{
	index = lua_absindex(L, index);
	lua_checkstack(L, 2);
	if (lclass_isobject(L, index)) {
		if (lua_getmetatable(L, index)) {
			luaL_getmetatable(L, "lclass.instance");
			if (lua_rawequal(L, -1, -2)) {
				lua_pop(L, 2);
				return TRUE;
			}
		}
	}
	return FALSE;
}

int lclass_inheritsfrom (lua_State *L, int index)
{
	index = lua_absindex(L, index);
	if (!lclass_isobject(L, index) || !lclass_isobject(L, -1)) {
		return FALSE;
	}
	lua_getfield(L, index, "__is_a");
	lua_pushvalue(L, -2);
	lua_gettable(L, -2);
	int r = lua_isboolean(L, -1) && lua_toboolean(L, -1);
	lua_pop(L, 2);
	return r;
}
