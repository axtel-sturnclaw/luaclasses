-- class.lua - Lua OOP Class module.

-------------------------------------------------------------------------------

-- Settings:

-- Should we cache super.* lookups in the instance?  This trades performance for memory.
-- NOTE: Cached variables do *not* update when the source variable is assigned a new value.
local enable_caching = true

-- Should we call the constructor on the class, passing in the new instance and arguments, or
--  should we call the ctor on the instance, passing in the args?
local call_ctor_on_class = false

-- Should class construction automatically call all constructor methods in the class hierarchy,
--  or should we only call the first and let users manually call self.super:[ctor]() as needed.
local auto_ctor_chaining = false

-- The name of class constructor functions.  Defaults to __init in keeping with lua's
--  __[something] naming of meta-functions.
local ctor_name = '__init'

-- Perform a breadth-first search through the inheritance tree.
-- Enabled, the searcher operates in top-to-bottom mode, walking each branch round-robin style,
--  one level at a time.
-- Disabled, the searcher operates in left-to-right mode, recursively walking each branch in it's
--  entirety before walking the next branch.
local breadthfirstsearch = true

-------------------------------------------------------------------------------

local re = rawequal
local remove = table.remove
local error = _G.error

-- Forward declare some functions to prevent extraneous memory use.

local searchsuper

if breadthfirstsearch then
	-- Perform a breadth-first search
	function searchsuper(class, k, raw)
		local searcht = {raw and class or class.__super}
		local i = 1
		while #searcht > 0 do
			local super = table.remove(searcht, i)
			for _, class in ipairs(super) do
				local r = rawget(class, k); if r~=nil then return r, class end
				if #class.__super > 0 then table.insert(searcht, i, class.__super); i = i+1 end
			end
			-- finished this depth, wrap around to the next.
			if i>#searcht then i = 1 end
		end
	end
else
	-- Perform an order-first search
	function searchsuper(class, k, raw)
		if raw then class = {__super = class} end
		if #class.__super > 0 then
			for _, class in ipairs(class.__super) do
				local r, cls = rawget(class, k) if r ~= nil then return r, class end
				r, cls = searchsuper(class, k) if r ~= nil then return r, cls end
			end
		end
	end
end

-- Run class constructors for the new instance.
local function doctor (class, instance, ...)
	local args, ctor = {...}
	local function recurse(class)
		for _, v in ipairs(class.super) do recurse(v) end
		local ctor = rawget(class, ctor_name)
		if type(ctor) == "function" then
			if call_ctor_on_class then ctor(class, instance, table.unpack(args))
			else instance.super[class][ctor_name](instance, table.unpack(args)) end
		end
	end
	if auto_ctor_chaining then
		recurse(class)
	else
		if call_ctor_on_class then ctor = class[ctor_name]; if type(ctor) == "function" then
			ctor(class, instance, ...) end
		else ctor = instance[ctor_name]; if type(ctor) == "function" then
			ctor(instance, ...) end
		end
	end
end

local function instanceof (self, ...)
	local supers = {...}
	if #supers == 0 then return nil, "No superclasses passed!" end
	for _, v in ipairs(supers) do
		if not self.__is_a[v] then return false end
	end
	return true
end

local operator_t = {
	'__add', '__sub', '__mul', '__div', '__mod', '__pow', '__unm', '__idiv',
	'__band', '__bor', '__bxor', '__bnot', '__shl', '__shr',
	'__concat', '__len', '__lt', '__le',
}
local function copyoperators(t)
	for k,v in pairs(operator_t) do if not t[k] then t[k] = v end
	end
end

do
	local function getfunc(obj, key)
		if getmetatable(obj) ~= 'class' and getmetatable(obj) ~= 'instance' then
			return end
		local func = obj[key]; return type(func) == 'function' and func or nil
	end

	local function makemethod(op, extra)
		operator_t[op] = function (a,b)
			local mm = getfunc(a, op) or getfunc(b, op) or extra or
			function () error('Attempt to call undefined operation: '..op) end
		return mm(a,b) end
	end

	for i = 1, #operator_t do makemethod(operator_t[i]); operator_t[i] = nil end

	makemethod('__eq', function (a, b) return rawequal(a,b) end)

	makemethod('__gc', function () end)

	makemethod('__tostring', function(self) return
		("%s %s(super: %s)"):format(self.__type, type(self.__name) == 'string' and
			self.__name.." " or '', tostring(self.super))
	end)

	operator_t.__call = function (self, ...) return (getfunc(self, '__call') or function()
		error('Attempt to call a table value!', 0) end)(self, ...) end
end

local error = _G.error
local __supermt; __supermt = {
-- calls to self.super:... propagate the instance upwards so every function invoked as
--  self.super:... will operate on the calling instance or class, rather than the class
--  the function was defined in.
__index = function (self, key)
	if tonumber(key) then return nil
	-- Use self.super[superclass]:something() for multiple inheritance.
	elseif getmetatable(key) == 'class' and rawget(self, 0):instanceof(key) then
		local newsuper = {[0] = rawget(self, 0), [1] = key}
		return setmetatable(newsuper, __supermt)
	end
	local val, class = searchsuper(self, key, true)
	if type(val) ~= 'function' then return val end
	return function(...)
		local obj = rawget(self, 0)
		if re(select('1', ...), self) or re(select('1', ...), obj) then
			local oldsuper, oldobj = obj.super, class.super[0]
			obj.super = class.super; class.super[0] = obj
			-- Errors here can cause problems; catch them.
			local err
			local msgh = function(e)
				if type(e) == 'table' and e.__auth == 'supermt' then err = e
				else err = {
					debug.traceback(tostring(e), 1), __auth = 'supermt'
				} end
			end
			local ret = table.pack(xpcall(val, msgh, obj, select('2', ...)))
			obj.super = oldsuper; class.super[0] = oldobj
			-- Rethrow the error; unfortunately renders error traceback unhelpful.
			if not ret[1] and obj.super ~= obj.__super then	error(err, 0)
			elseif not ret[1] and obj.super == obj.__super then error(err[1], 0)
			else return table.unpack(ret, 2, ret.n) end
		else
			return val(...)
		end
	end
end,
__newindex = function() -- left empty to prevent assignment to super tables.
end,
__metatable = 'super'
}

local __instmt = {
--  Cache the value in the instance for quick lookup.
__index = function(inst, k)
	local v = inst.__super[k]
	if v ~= nil and not re(k, ctor_name) and enable_caching then
		inst[k] = v end
	return v end,
__metatable = 'instance'
}; copyoperators(__instmt)

local __classmt = {
-- Construct a new instance.
__call = function (class, ...)
	local instance = setmetatable({}, __instmt); instance.__type = "instance"
	instance.super = setmetatable({[0] = instance, class}, __supermt)
	-- Calls to super.* are explicitly looking for a value 'up' the function's place
	-- in the hierarchy, but calls to instance.* are not.  This allows both to work.
	instance.__super = instance.super
	doctor(class, instance, ...)
	return instance
end,
__index = function(self, key)
	return self.__super[key] end,
__metatable = 'class'
}; copyoperators(__classmt)

-------------------------------------------------------------------------------

local function class (...)
	local nclass, args = {}, {...}
	nclass.__type = "class"
	nclass.__is_a, nclass.super = {}, {[0] = nclass}
	nclass.__is_a[nclass] = true
	local name = args[1]
	if type(name) == 'string' and not re(name, 'final') then
		nclass.__name = name
		remove(args, 1)
	end

	local final = args[#args]
	if re(final, "final") or re(final, true) then
		nclass.__final = true
		remove(args, #args)
	end

	local bases = args
	for _, base in ipairs(bases) do
		if getmetatable(base) == 'class' and not rawget(base, '__final') then
			table.insert(nclass.super, base)
			nclass.__is_a[base] = true
			for c in pairs(base.__is_a) do nclass.__is_a[c] = true end
		end
	end

	nclass.instanceof = instanceof

	setmetatable(nclass.super, __supermt); nclass.__super = nclass.super
	return setmetatable(nclass, __classmt)
end

-------------------------------------------------------------------------------

return class
