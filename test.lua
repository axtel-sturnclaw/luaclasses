#! /usr/local/bin/lua
-- Lua class module unit tests.
-- Big mess of spaghetti code, unfortunately.
class = select('1', ...) and require 'lclass' or dofile 'init.lua'

a = class()
b = class()
c = class()
print(getmetatable(a))
a.test1 = "abcde"
b.test2 = "fghij"
c.test3 = "klmno"

sc1 = class(a,b,c,"final")
print(sc1:instanceof(a), sc1():instanceof(b), sc1():instanceof())
print("Subclass demonstration")
i = sc1()
print(i.test1..i.test2..i.test3)

print("Inheritance denial with final classes")
ft = class(sc1)
print(sc1.test1, ft.test1)

print("Superclass access, pt. 1: Variables")
i.test2 = "test"
print(i.test2, i.super.test2)

print("Superclass access, pt. 2: Functions")
function b:g(data) print(self, 'superclass function', data) end
function i:g(data) print(self, 'instance function', data); self.super:g(data) end
print("Instance: "..tostring(i),"Superclass: "..tostring(b))
i:g("function 'g'")

print("super:...() chaining")
a = class()
b = class(a)
c = class(b)
d = class(c)
b.test = "class-test"

function b:__init(msg) self.test = msg end
function c:__init(msg) self.super:__init(msg) end

function a:tick(msg) print("a:tick", self.test, msg) end
function b:tick(msg) self.super:tick(msg); print("b:tick", msg) end
function c:tick(msg) self.super:tick(msg); print("c:tick", msg) end
i = d("instance-test")

print("Superclass functions acting on a class...")
d:tick("class")
print("Superclass functions acting on an instance...")
i:tick("instance")

print("Custom operator definitions: ")
function a:__add(t) return self.m + t.m end
function b.__mul(r,t) return r.m * t.m end
function c:__mul(b) return self.super:__mul(b) end
function d:__eq(b) return self.m == b.m end
function d:__call(...) print(self, ...) end

d.m = 3
i2 = d(); i2.m = 4
print(string.format('%d + %d = %d', i.m, i2.m, i + i2))
print(string.format('%d * %d = %d', i.m, i2.m, i * i2))

i('test', 'test2', 'test3')

if false then
print("Error traceback test:")
function a:error() error("test") end
function b:error() return self.super:error() end

i:error()
end

a = class()
b = class(a)
c = class(a)
d = class(b,c)
function a:__gc() print(self) end
function a:test(caller) print('a:test() called from:', caller) end
function b:test() self.super:test('b') end
function c:test() self.super:test('c') end
function d:test() self.super[b]:test() self.super[c]:test() print('d calls both!') end

i = d(); i:test()

print 'EOF GC:'
