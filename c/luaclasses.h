/*
 * LuaClasses.h - Lua Class Module API
 * Copyright 2016 Web eWorks, LTD
 */

#ifndef LUACLASSES_H
#define LUACLASSES_H

#include <string.h>
#include <lua.h>
#include <lauxlib.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef LUACLASSES_BFIRST
#define LUACLASSES_BFIRST 1
#endif

#ifndef LUACLASSES_CTORNAME
#define LUACLASSES_CTORNAME "__init"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================= */

/*
 * Terminology Note:
 * `class` strictly means a class created by this module.
 * `instance` strictly means an `instance` of a `class` created by this module.
 * `class object` means any object, class or instance, created by this module.
 */

/* ========================================================================= */

/*
 * Traverses the superclass hierarchy of the object at the specified index,
 * looking for the key at the top of the stack.  Pops the key from the stack if
 * the value is found, pushing the value and class in which it was found
 * instead.
 * If raw is non-zero, the object at index is treated as a super table
 * instead of a class object.
 */
int lclass_searchsuper(lua_State *L, int index, int raw);

/*
 * Add operator redirection functions to the table on the top of the stack.
 */
int lclass_setoperators(lua_State *L);

/*
 * Create a new class inheriting from `nsuper` superclasses.
 * If with_name is not 0, sets the __name field of the new class to the
 * value before the superclasses.
 * Pops the superclasses, and pushes the new class onto the stack.
 */
int lclass_newclass(lua_State *L, int nsuper, int with_name, int final);

/*
 * Creates a new instance from the class at the specified index.
 * Does not perform instance initialization, just creates the class.
 * Pushes the resulting instance onto the stack without popping the class.
 */
int lclass_newinstance(lua_State *L, int index);

/*
 * Initializes the instance at the specified index, passing the first `numvars`
 * variables from the top of the stack.
 * Does NOT catch errors in initialization functions.
 * Returns TRUE if successful.
 */
int lclass_initinstance(lua_State *L, int index, int numvars);

/*
 * Returns TRUE if the value at the given index is a class object
 */
int lclass_isobject(lua_State *L, int index);

/*
 * Returns TRUE if the value at the given index is an actual class.
 */
int lclass_isclass(lua_State *L, int index);

/*
 * Returns TRUE if the value at the given index is a class instance.
 */
int lclass_isinstance(lua_State *L, int index);

/*
 * Returns true if the value at the given index is a class object inheriting from
 * the class object at the top of the stack.
 * Returns FALSE if either object is not a class object, or if there is no inheritance.
 * Does NOT pop anything from the stack.
 */
int lclass_inheritsfrom(lua_State *L, int index);

#ifdef __cplusplus
}
#endif

#endif
