/*
 * LuaClasses_Operators.c - Class Operator Redirectors
 * Copyright 2016 Web eWorks, LTD
 */

#include <stdio.h>
#include "luaclasses.h"

static int getfunc (lua_State *L, int objindex, const char * name)
{
	if (lclass_isobject(L, objindex)) {
		if (lua_getfield(L, objindex, name) == LUA_TFUNCTION)
			return 1;
		else
			lua_pop(L, 1);
	}

	return 0;
}

static int __cont (lua_State *L, int status, lua_KContext ctx)
{
	(void)ctx;
	return lua_gettop(L);
}

static int opfunc (lua_State *L)
{
	int args = lua_gettop(L);
	const char * name = lua_tostring(L, lua_upvalueindex(1));
	if (!getfunc(L, 1, name)){
		if (!getfunc(L, 2, name)){
			if (strcmp(name, "__eq") == 0) {
				lua_pushboolean(L, lua_rawequal(L, 1,2));
				return 1;
			}
			else if (strcmp(name, "__gc") == 0) {
				return 0;
			}
			else if (strcmp(name, "__tostring") == 0 && lclass_isobject(L, 1)) {
				lua_settop(L, 1);
				lua_getfield(L, 1, "__type");
				lua_pushfstring(L, " ");

				if (lua_getfield(L, 1, "__name") == LUA_TSTRING)
					lua_pushfstring(L, " ");
				else
					lua_pop(L, 1);

				lua_pushfstring(L, "(super: ");
				lua_getfield(L, 1, "super");
				luaL_tolstring(L, -1, NULL);
				lua_remove(L, -2);
				lua_pushfstring(L, ")");
				lua_concat(L, lua_gettop(L) - 1);
				return 1;
			}
			else luaL_error(L, "Attempted undefined operation %s", name);
		}
	}
	lua_insert(L, 1);
	lua_callk(L, args, LUA_MULTRET, 0, __cont);
	return lua_gettop(L);
}

const char *names[] = {
	"add", "sub", "mul", "div",
	"mod", "pow", "unm", "idiv",
	"band", "bor", "bxor", "bnot",
	"shl", "shr", "concat", "len",
	"eq", "lt", "le", "call", "tostring",
	"gc",
	NULL
};

int lclass_setoperators(lua_State *L)
{
	if (!lua_istable(L, -1)) return 0;

	for (int i = 0; names[i] != NULL; ++i) {
		char name[16] = "__";
		strcat(name, names[i]);
		int indexval = lua_getfield(L, -1, name);
		lua_pop(L, 1);

		/* Don't overwrite existing values. */
		if (indexval != LUA_TNIL) continue;
		lua_pushstring(L, name);
		lua_pushcclosure(L, opfunc, 1);
		lua_setfield(L, -2, name);
	}

	return 1;
}
